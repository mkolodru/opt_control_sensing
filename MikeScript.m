M = csvread('PoggialiPowerDensitySpectrum.csv'); % M(i,1) = time in seconds, M(i,2) = coherence time of signal from Poggiali paper used to determine the power density spectrum
for i = 1:length(M(:,1))
    S(1,i) = 2*pi*M(i,1); % = omega in s^-1
    S(2,i) = 2*pi*M(i,2); % = S(omega)
    %y = yofomega(S(1,i),tau,n); %This is y(omega)
    %S(3,i) = S(2,i)*abs(y)^2; %This is the set of quantities that are integrated over in order to obtain Theta_beta My Way
    %S(3,i) = S(2,i)*abs(y)^4/(S(1,i))^2; %This is the set of quantities that are integrated over in order to obtain Theta_beta Not My Way
end
fun = @(a)a(1)+a(2)*exp(-((S(1,:)-a(3))/a(4)).^2)+a(5)*exp(-((S(1,:)-a(6))/a(7)).^2)+a(8)*exp(-((S(1,:)-a(9))/a(10)).^2)+a(11)*exp(-((S(1,:)-a(12))/a(13)).^2)+a(14)*exp(-((S(1,:)-a(15))/a(16)).^2)-S(2,:);
start = [0.01*10^6 0.1*10^6 0.5*10^6 0.05*10^6 0.3*10^6 0.7*10^6 0.05*10^6 2.75*10^6 2.75*10^6 0.2*10^6 0.6*10^6 2.95*10^6 0.1*10^6 0.25*10^6 4.5*10^6 0.3*10^6];
low = [0.001*10^6 0.02*10^6 0.35*10^6 0.01*10^6 0.2*10^6 0.6*10^6 0.01*10^6 2.5*10^6 2.5*10^6 0.1*10^6 0.5*10^6 2.9*10^6 0.03*10^6 0.1*10^6 4.2*10^6 0.1*10^6];
up = [0.1*10^6 0.25*10^6 0.6*10^6 0.1*10^6 0.4*10^6 0.85*10^6 0.1*10^6 3*10^6 2.9*10^6 0.5*10^6 0.8*10^6 3*10^6 0.3*10^6 0.5*10^6 4.8*10^6 1*10^6];
options = optimoptions(@lsqnonlin,'Algorithm','trust-region-reflective');
a = lsqnonlin(fun,start,low,up,options);