import numpy
import pylab
data_pog=numpy.genfromtxt('PoggialiPowerDensitySpectrum.csv',delimiter=',')
omega=data_pog[:,0]
S_pog=data_pog[:,1]
import read_S
S_me=read_S.get_S(omega)
pylab.plot(omega,S_pog,label='S_pog')
pylab.plot(omega,S_me,label='S_me')
pylab.legend(loc='best')
pylab.show()
