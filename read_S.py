import numpy

S_params=numpy.loadtxt('S_params.dat')

def get_S(omega):
    j=0
    S=S_params[j]
    for j in range(1,len(S_params),3):
        S+=S_params[j]*numpy.exp(-((omega-S_params[j+1])/S_params[j+2])**2)
    return S

